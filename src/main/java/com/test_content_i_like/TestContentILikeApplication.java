package com.test_content_i_like;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestContentILikeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestContentILikeApplication.class, args);
    }

}
